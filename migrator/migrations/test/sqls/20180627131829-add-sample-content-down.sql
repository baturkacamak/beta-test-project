-- noinspection SqlWithoutWhereForFile

/* Replace with your SQL commands */

DELETE from content WHERE id > 5;
DELETE from "file";

ALTER SEQUENCE content_id_seq RESTART WITH 1;
ALTER SEQUENCE file_id_seq RESTART WITH 1;
