#!/bin/bash -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$(cd ${SCRIPT_DIR}/..; pwd)

VERSION=$(git branch | grep \* | cut -d ' ' -f2)

cd ${ROOT_DIR}

COMPONENTS=('api' 'migrator' 'admin' 'frontend')

for COMPONENT in "${COMPONENTS[@]}"; do

    echo "Building ${COMPONENT}..."
    docker build -t repo.treescale.com/betabcn/berta-models/${COMPONENT}:${VERSION} -f deploy/${COMPONENT}/Dockerfile .

    echo "Pushing ${COMPONENT}..."
    docker push repo.treescale.com/betabcn/berta-models/${COMPONENT}:${VERSION}

done

