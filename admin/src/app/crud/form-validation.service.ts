import {Injectable} from '@angular/core';
import {ApolloError} from 'apollo-client';
import {FormGroup} from '@angular/forms';
import {ToastrService} from "ngx-toastr";

interface ValidationError {
  constraints: {
    [index: string]: string
  }
  property: string
  value: string
  target: any
}

@Injectable()
export class FormValidationService {

  constructor(private readonly toastrService: ToastrService){}

  processErrors(err: ApolloError, form: FormGroup) {

    const { graphQLErrors } = err;

    if(!graphQLErrors) return;

    for(const graphQLError of err.graphQLErrors) {

      const validationErrors = graphQLError['validationErrors'] as ValidationError[];
      if(!validationErrors) return;

      for(const validationError of validationErrors) {

        const { property, constraints } = validationError;

        const control = form.get(property);
        control.setErrors(constraints);

      }

    }

    this.toastrService.error('Errors exist in the form, please review');

  }

}
