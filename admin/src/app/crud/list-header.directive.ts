import {Directive} from '@angular/core';


@Directive({
  selector: '[listHeader]'
})
export class ListHeaderDirective {}
