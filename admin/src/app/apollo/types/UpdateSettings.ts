/* tslint:disable */
// This file was automatically generated and should not be edited.

import { UpdateSetting } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: UpdateSettings
// ====================================================

export interface UpdateSettings_updateSettings {
  __typename: "Setting";
  id: string | null;
  value: string | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UpdateSettings {
  updateSettings: (UpdateSettings_updateSettings | null)[];
}

export interface UpdateSettingsVariables {
  settings?: (UpdateSetting | null)[] | null;
}
