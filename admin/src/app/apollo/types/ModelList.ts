/* tslint:disable */
// This file was automatically generated and should not be edited.

import { PublishState } from "./globalTypes";

// ====================================================
// GraphQL query operation: ModelList
// ====================================================

export interface ModelList_contentsAdmin_modelContent {
  __typename: "ModelContent";
  name: string | null;
  email: string | null;
}

export interface ModelList_contentsAdmin {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  publishedAt: any | null;
  modelContent: ModelList_contentsAdmin_modelContent | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ModelList {
  contentsAdmin: (ModelList_contentsAdmin | null)[];
  contentsAdminCount: number;
}

export interface ModelListVariables {
  offset?: number | null;
  limit?: number | null;
  q?: string | null;
  state?: PublishState | null;
}
