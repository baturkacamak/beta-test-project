/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentTypeList
// ====================================================

export interface ContentTypeList_contentTypes {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ContentTypeList {
  contentTypes: (ContentTypeList_contentTypes | null)[];
  contentTypesCount: number;
}

export interface ContentTypeListVariables {
  offset?: number | null;
  limit?: number | null;
  q?: string | null;
}
