/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentDetailBySlug
// ====================================================

export interface ContentDetailBySlug_contentBySlugAdmin_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
}

export interface ContentDetailBySlug_contentBySlugAdmin {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  publishedAt: any | null;
  contentType: ContentDetailBySlug_contentBySlugAdmin_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ContentDetailBySlug {
  contentBySlugAdmin: ContentDetailBySlug_contentBySlugAdmin | null;
}

export interface ContentDetailBySlugVariables {
  slug: string;
}
