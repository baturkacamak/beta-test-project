/* tslint:disable */
// This file was automatically generated and should not be edited.

import { UpsertUser } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: UpdateUserById
// ====================================================

export interface UpdateUserById_updateUserById {
  __typename: "User";
  id: string | null;
  email: string | null;
  firstName: string | null;
  familyName: string | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UpdateUserById {
  updateUserById: UpdateUserById_updateUserById;
}

export interface UpdateUserByIdVariables {
  id: string;
  user?: UpsertUser | null;
}
