import {OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';

export abstract class BaseComponent implements OnDestroy {

  protected subs: Subscription[] = [];

  protected subscription(fn: () => Subscription | Subscription[]) {

    const result = fn();

    if (result instanceof Array){
      this.subs = this.subs.concat(result);
    } else {
      this.subs.push(result);
    }

  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }

}
