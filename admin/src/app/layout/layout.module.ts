import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MainComponent} from './main.component';
import {NavbarComponent} from './navbar.component';
import {SidebarComponent} from './sidebar.component';
import {IconsModule} from '../icons.module';

@NgModule({
  imports: [
    RouterModule,
    IconsModule
  ],
  declarations: [
    MainComponent,
    NavbarComponent,
    SidebarComponent
  ]
})
export class LayoutModule {

}
