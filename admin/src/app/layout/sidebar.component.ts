import {Component} from '@angular/core';

@Component({
  selector: 'app-sidebar',
  template: `
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a routerLink="/dashboard" class="nav-link" routerLinkActive="active">
              <fa-icon icon="tachometer-alt" [fixedWidth]="true"></fa-icon>
              Dashboard
            </a>
          </li>

          <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Content</span>
          </h6>

          <li class="nav-item">
            <a routerLink="/content/models" class="nav-link" routerLinkActive="active">
              <fa-icon icon="home" [fixedWidth]="true" ></fa-icon>
              Models
            </a>
          </li>
          
          <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>General</span>
          </h6>

          <li class="nav-item">
            <a routerLink="/users" class="nav-link" routerLinkActive="active">
              <fa-icon icon="users" [fixedWidth]="true"></fa-icon>
              Users
            </a>
            <a routerLink="/settings" class="nav-link" routerLinkActive="active">
              <fa-icon icon="cogs" [fixedWidth]="true"></fa-icon>
              Settings
            </a>
          </li>
          
        </ul>
      </div>
    </nav>`
})
export class SidebarComponent {

}
