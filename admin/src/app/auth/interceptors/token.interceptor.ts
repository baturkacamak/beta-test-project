import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {NGXLogger} from 'ngx-logger';
import {catchError, map} from 'rxjs/operators';
import {_throw} from 'rxjs/observable/throw'
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {AuthService} from '@app/auth/auth.service';
import {Apollo} from 'apollo-angular';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private readonly router: Router,
              private readonly apollo: Apollo,
              private readonly toastr: ToastrService,
              private readonly log: NGXLogger,
              private readonly authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const {log, authService, toastr} = this;

    log.debug('[Token interceptor] Intercepting request', request);

    const accessToken = authService.getAccessToken();

    if (!accessToken) {
      log.debug('[Token interceptor] No access token detected');
    } else {

      log.debug('[Token interceptor] Setting authorization header');

      request = request.clone({
        headers: request.headers.set('authorization', `Bearer ${accessToken.id}`)
      });

    }

    return next.handle(request)
      .pipe(
        map((resp: HttpResponse<any>) => {

          // examine apollo response and detect a 401

          const { body } = resp;
          if(!body) return resp;

          const { errors } = resp.body;
          if(!(errors && errors.length)) return resp;

          errors.forEach(err => {
            const { message } = err;
            // re-throw as http error response
            if(message.statusCode === 401 || message === 'jwt expired'){
              throw new HttpErrorResponse({ status: 401, error: message.error });
            }
          });

          return resp;
        }),
        catchError(err => {

          log.error('[Token interceptor] Error event', err);

          if (err instanceof HttpErrorResponse && err.status === 401) {

            log.error('[Token interceptor] Http error response detected', err);
            toastr.warning('It appears your session has expired. Please login again');

            authService.logout();

          }

          // re-throw the error
          return _throw(err);
        }));

  }
}
