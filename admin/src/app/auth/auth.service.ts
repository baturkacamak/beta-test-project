import {Inject, Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {Router} from '@angular/router';
import {SESSION_STORAGE, StorageService} from 'ngx-webstorage-service';



@Injectable()
export class AuthService {

  constructor(private readonly router: Router,
              private readonly apollo: Apollo,
              @Inject(SESSION_STORAGE)
              private readonly storage: StorageService) {
  }

  getAccessToken(): { id: string, expiresIn: number } {
    return this.storage.get('accessToken');
  }

  setAccessToken(id: string, expiresIn: number) {
    this.storage.set('accessToken', { id, expiresIn });
  }

  clearAccessToken() {
    this.storage.set('accessToken', null);
  }

  async logout() {

    await this.apollo.getClient().resetStore();

    this.clearAccessToken();
    this.router.navigateByUrl('/login');
  }

}
