import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LayoutModule} from "../layout/layout.module";
import {ReactiveFormsModule} from "@angular/forms";
import {NgbPaginationModule} from "@ng-bootstrap/ng-bootstrap";
import {FormlyModule} from "@ngx-formly/core";
import {TimeagoModule} from "ngx-timeago";
import {IconsModule} from "../icons.module";
import {CrudModule} from "../crud/crud.module";
import {SettingListComponent} from "./setting-list.component";
import {SettingRoutesModule} from "./setting.routes";
import {SettingsComponent} from "./settings.component";

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    FormlyModule,
    TimeagoModule,
    IconsModule,
    CrudModule,
    SettingRoutesModule
  ],
  declarations: [
    SettingListComponent,
    SettingsComponent
  ]
})
export class SettingModule {

}
