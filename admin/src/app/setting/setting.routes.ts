import {MainComponent} from '../layout/main.component';
import {AuthenticatedGuard} from '../auth/guards/authenticated.guard';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SettingsComponent} from "./settings.component";

export const settingRoutes: Routes = [
  {
    path: 'settings',
    component: MainComponent,
    canActivate: [AuthenticatedGuard],
    children: [
      {
        path: '',
        component: SettingsComponent,
        data: {
          title: 'Settings'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(settingRoutes)],
    exports: [RouterModule]
})
export class SettingRoutesModule {}
