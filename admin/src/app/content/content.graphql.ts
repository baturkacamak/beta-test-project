import gql from 'graphql-tag';

export const contentListFragment = gql`
  fragment ContentListView on Content {
    id
    title
    meta
    slug
    data
    publishedAt
    contentType {
      id
      name
    }
    createdAt
    updatedAt
  }
`;

export const contentDetailFragment = gql`
  fragment ContentDetailView on Content {
    id
    title
    meta
    slug
    data
    publishedAt
    contentType {
      id
      name
      slug
      formConfig
    }
    createdAt
    updatedAt
  }
`;

export const modelContentListQuery = gql`
  fragment ModelContentListView on Content {
    id
    title
    meta
    slug
    publishedAt
    modelContent {
      name
      email
    }
    createdAt
    updatedAt
  }
`;

export const contentListQuery = gql`
  query ContentList($offset: Int, $limit: Int, $q: String){
    contentsAdmin(offset: $offset, limit: $limit, q: $q) {
      ...ContentListView
    }
    contentsAdminCount(q: $q)
  }
  ${contentListFragment}
`;

export const modelListQuery = gql`
  query ModelList($offset: Int, $limit: Int, $q: String, $state: PublishState){
    contentsAdmin(offset: $offset, limit: $limit, q: $q, contentType: "model", state: $state) {
      ...ModelContentListView
    }
    contentsAdminCount(q: $q, contentType: "model", state: $state)
  }
  ${modelContentListQuery}
`;


export const contentDetailQuery = gql`
  query ContentDetail($id: ID!) {
    contentAdmin(id: $id) {
      ...ContentDetailView
    }
  }
  ${contentDetailFragment}
`;




export const contentDetailBySlugQuery = gql`
  query ContentDetailBySlug($slug: String!) {
    contentBySlugAdmin(slug: $slug) {
      ...ContentDetailView
    }
  }
  ${contentDetailFragment}
`;

export const contentDetailByIdQuery = gql`
  query ContentDetailById($id: ID!) {
    contentAdmin(id: $id) {
      ...ContentDetailView
    }
  }
  ${contentDetailFragment}
`;

export const createContentMutation = gql`
  mutation CreateContent($content: UpsertContent) {
    createContent(content: $content) {
      ...ContentDetailView
    }
  }
  ${contentDetailFragment}
`;

export const updateContentByIdMutation = gql`
  mutation UpdateContentById($id: ID!, $content: UpsertContent) {
    updateContentById(id: $id, content: $content) {
      ...ContentDetailView
    }
  }
  ${contentDetailFragment}
`;


export const deleteContentByIdMutation = gql`
  mutation DeleteContentById($id: ID!) {
    deleteContentById(id: $id)
  }
`;
