import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AuthModule} from './auth/auth.module';
import {AuthenticatedGuard} from './auth/guards/authenticated.guard';
import {MainComponent} from './layout/main.component';
import {DashboardComponent} from './dashboard.component';

const appRoutes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {
    path: 'dashboard',
    component: MainComponent,
    canActivate: [AuthenticatedGuard],
    data: {
      title: 'Dashboard'
    },
    children: [
      {
        path: '',
        component: DashboardComponent
      }
    ]
  },
];

@NgModule({
  imports: [
    AuthModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
