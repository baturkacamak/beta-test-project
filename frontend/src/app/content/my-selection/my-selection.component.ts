import {Component, OnInit} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {createFormSubmissionMutation, selectedModelsQuery} from '@app/app.graphql';
import {SelectedModelsQuery, SelectedModelsQuery_selectedModels,} from '@app/apollo/types/SelectedModelsQuery';
import {ActivatedRoute} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {ContentByIdQuery} from '@app/apollo/types/ContentByIdQuery';
import {SelectionCookieService} from "@app/shared/services/selection-cookie.service";
import {ToastrService} from "ngx-toastr";
import {FormGroup} from "@angular/forms";
import {FormConfigByIdQuery_formConfig} from "@app/apollo/types/FormConfigByIdQuery";
import {FormlyFieldConfig} from "@ngx-formly/core";
import {cloneDeep} from 'apollo-utilities';
import {FormSubmissionView} from "@app/apollo/types/FormSubmissionView";

@Component({
  selector: 'app-my-selection',
  templateUrl: 'my-selection.component.html',
  styleUrls: ['my-selection.component.sass'],
})
export class MySelectionComponent implements OnInit {
  selectedModels: SelectedModelsQuery_selectedModels[];
  title: string;

  form: FormGroup;

  formConfig: FormConfigByIdQuery_formConfig;
  formFields: FormlyFieldConfig[];

  data: {};
  submitted: boolean;

  constructor(
    private readonly apollo: Apollo,
    private readonly route: ActivatedRoute,
    private readonly titleService: Title,
    private readonly metaService: Meta,
    private cookieService: SelectionCookieService,
    private readonly toastr: ToastrService
  ) {}

  ngOnInit(): void {

    const { data } = this.route.snapshot;

    const { content } = data.content as ContentByIdQuery;
    this.formConfig = data.form.formConfig as FormConfigByIdQuery_formConfig;

    const { title, meta } = content;

    this.title = title;

    this.formFields = cloneDeep(this.formConfig.config['fields'] as FormlyFieldConfig[]);

    // SEO

    this.titleService.setTitle(`Berta Models - ${title}`);

    meta.forEach(m => {
      const { key, value } = m;
      this.metaService.updateTag({ name: key, content: value });
    });

    this.fetchSelectedModels();

    // Subscribe to changes in selected models
    this.cookieService.selectionChanged.subscribe(() => {
      this.fetchSelectedModels();
    });

    this.form = new FormGroup({});
    this.data = {};
  }

  fetchSelectedModels() {

    const ids = this.cookieService.getSelectedIds();

    if (!ids.length) {
      return this.selectedModels = [];
    }

    this.apollo
      .query<SelectedModelsQuery>({
        query: selectedModelsQuery,
        variables: { ids },
      })
      .subscribe(
        results => {
          this.selectedModels = results.data
            .selectedModels as SelectedModelsQuery_selectedModels[];
        },
        err => {
          this.toastr.error('Failed to fetch selected models.', 'Error');
        },
      );
  }

  onSubmit() {

    const valid = this.validateForm();

    if (!valid) {
      return;
    }

    const models = this.selectedModels
      .map(model => {
        return {...model.modelContent, slug: model.slug}
      }) as any;

    for (let model of models) {
      model.images = [model.images[0]]; // Only send one image to be sent via email
      model.images = model.images.map(image => {
        return {
          id: image.id,
          metadata: {
            secure_url: image.metadata['secure_url']
          }
        }
      });
    }

    const payload = {
      formConfigId: this.formConfig.id,
      data: {...this.data, models}
    };

    this.apollo.mutate<FormSubmissionView>({
      mutation: createFormSubmissionMutation,
      variables: {formSubmission: payload}
    })
      .subscribe(
        () => {
          this.submitted = true;
        },
        (err) => {
          this.toastr.error(err.message, 'Error');
        },
      )

  }

  validateForm() {
    return !this.form.invalid;
  }


}
