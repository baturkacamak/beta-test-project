import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Inject,
  OnInit,
  ViewChild
} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Meta, Title} from '@angular/platform-browser';
import {WINDOW} from '@app/window.service';
import {LayoutService} from '@app/layout/layout.service';
import {
  ModelContentDetail_content,
  ModelContentDetail_content_modelContent,
} from '@app/apollo/types/ModelContentDetail';
import {SelectionCookieService} from "@app/shared/services/selection-cookie.service";
import {environment} from "@src/environments/environment";
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'model-content',
  templateUrl: 'model-content.component.html',
  styleUrls: ['model-content.component.sass'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({opacity: 0}),
        animate('.5s', style({opacity: 1})),
      ]),
      transition(':leave', [animate('.5s', style({opacity: 0}))]),
    ]),
  ],
})
export class ModelContentComponent implements OnInit, AfterViewChecked {

  title: string;
  body: string;
  id: string;
  selected: boolean;

  nameStyle: {};

  private scroll: any;
  showFloatingMenu: boolean;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.nameStyle = null;
    this.resizeNameText();
  }

  modelContent: ModelContentDetail_content_modelContent;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly titleService: Title,
    private readonly metaService: Meta,
    private readonly layoutService: LayoutService,
    private readonly cookieService: SelectionCookieService,
    @Inject(WINDOW) private readonly window: Window
  ) {
    this.scroll = (): void => {
      this.onScroll(window.pageYOffset);
    };
  }

  ngOnInit(): void {

    window.addEventListener('scroll', this.scroll, true);

    const {content} = this.route.snapshot.data;
    const {modelContent, meta, id} = content as ModelContentDetail_content;

    this.title = content.title;
    this.modelContent = modelContent;
    this.id = id;

    // SEO

    this.titleService.setTitle(`Berta Models - ${content.title}`);

    meta.forEach(m => {
      const {key, value} = m;
      this.metaService.updateTag({name: key, content: value});
    });

    this.window.scrollTo(0, 0);

    // Check if already selected
    this.selected = this.cookieService.getSelectedIds().indexOf(this.id) > -1;

  }

  ngAfterViewChecked(): void {
    this.onScroll(window.pageYOffset);

    this.resizeNameText();

  }

  resizeNameText() {

    // Dynamically adjust name text-size to fit within container

    const timer = setTimeout(() => {
      const modelNameContainer: HTMLElement = document.getElementById('modelNameContainer');
      if (!modelNameContainer) return clearTimeout(timer);
      const {scrollWidth, clientWidth} = modelNameContainer;

      if (scrollWidth > clientWidth) {
        const modelName: HTMLElement = document.getElementById('modelName');
        const fontSize = this.nameStyle ?
          this.nameStyle['font-size.px'] :
          parseFloat(window.getComputedStyle(modelName, null).getPropertyValue('font-size'));
        this.nameStyle = {'font-size.px': fontSize - 5};
      }
    })
  }

  urlBgImg = img => `url(${img})`;

  produceDlHtml() {
    return `<dl>
      <dt>Height</dt>
      <dd>${this.modelContent.height}</dd>
      <dt>Bust</dt>
      <dd>${this.modelContent.bust}</dd>
      <dt>Waist</dt>
      <dd>${this.modelContent.waist}</dd>
      <dt>Hips</dt>
      <dd>${this.modelContent.hips}</dd>
      <dt>Shoes</dt>
      <dd>${this.modelContent.shoes}</dd>
      <dt>Eye</dt>
      <dd>${this.modelContent.eyeColor}</dd>
      <dt>Hair</dt>
      <dd>${this.modelContent.hairColor}</dd>
    </dl>`;
  }

  onHeartClicked() {
    this.selected = !this.selected;

    const {id} = this;

    this.selected ?
      this.cookieService.addSelectedId(id) :
      this.cookieService.removeSelectedId(id);
  }

  generatePdfDownloadUrl(): string {
    return environment.apiUrl + '/content/' + this.id + '/pdf';
  }

  onScroll(offsetY: number) {

    const docBottom = offsetY + this.window.innerHeight;

    const footer = document.getElementById('berta-footer');

    if (!footer) return this.showFloatingMenu = true;

    const footerOffsetY = footer.offsetTop;

    this.showFloatingMenu = docBottom <= footerOffsetY; // Only show floating menu if footer is off-screen

  }

}
