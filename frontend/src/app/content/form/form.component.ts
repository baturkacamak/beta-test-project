import {Component, Inject, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Meta, Title} from "@angular/platform-browser";
import {FormlyFieldConfig} from "@ngx-formly/core";
import {FormConfigByIdQueryResult} from "@app/content/resolvers/form.resolver";
import {FormGroup} from "@angular/forms";
import {FormConfigByIdQuery_formConfig} from "@app/apollo/types/FormConfigByIdQuery";
import {Apollo} from "apollo-angular";
import {FormSubmissionView} from "@app/apollo/types/FormSubmissionView";
import {createFormSubmissionMutation, filesForEmailQuery} from "@app/app.graphql";
import {cloneDeep} from 'apollo-utilities';
import {FormContentByIdQuery_content_formContent} from "@app/apollo/types/FormContentByIdQuery";
import {ToastrService} from "ngx-toastr";
import {Observable} from "rxjs";
import {FileForEmailFragment} from "@app/apollo/types/FileForEmailFragment";
import {Files_files} from "@app/apollo/types/Files";
import {ApolloQueryResult} from "apollo-client";
import {WINDOW} from "@app/window.service";

@Component({
  selector: 'app-form',
  templateUrl: 'form.component.html',
  styleUrls: ['form.component.sass']
})
export class FormComponent implements OnInit {

  form: FormGroup;

  formContent: FormContentByIdQuery_content_formContent;
  formConfig: FormConfigByIdQuery_formConfig;
  formFields: FormlyFieldConfig[];

  data: {};
  submitted: boolean;

  constructor(
    private readonly apollo: Apollo,
    private readonly route: ActivatedRoute,
    private readonly titleService: Title,
    private readonly metaService: Meta,
    private readonly toastr: ToastrService,
    @Inject(WINDOW) private readonly window: Window
  ) {
  }

  ngOnInit(): void {

    const { query } = this.route.snapshot.data;

    const { content, formConfig } = query as FormConfigByIdQueryResult;
    const { meta } = content;

    this.formContent = content.data;

    this.formConfig = formConfig;

    this.formFields = cloneDeep(formConfig.config['fields'] as FormlyFieldConfig[]);
    this.form = new FormGroup({});
    this.data = {};

    // SEO

    this.titleService.setTitle(`Berta Models - ${content.title}`);

    meta.forEach(m => {
      const { key, value } = m;
      this.metaService.updateTag({ name: key, content: value });
    });

    this.window.scrollTo(0, 0);
  }

  onSubmit() {

    if (!this.validateForm()) {
      return;
    }

    const payload = {
      formConfigId: this.formConfig.id,
      data: {...this.data} as any
    };

    // Format form data for form submission dependent on form config type

    switch (this.formConfig.name) {
      case 'contact':
        this.fetchFilesMetadata(payload)
          .subscribe((files: ApolloQueryResult<Files_files[]>) => {

            // Format image data for sending via email
            const keys = ['shot1', 'shot2', 'shot3', 'shot4', 'shot5'];
            for (let key of keys) {

              const file = files.data['files'].find(f => f.id === payload.data[key].toString());
              payload.data[key] = file;
              payload.data[key].metadata = { secure_url: file.metadata.secure_url };
            }
            this.createFormSubmission(payload);

          }, err => this.toastr.error(err, 'Error'));
        break;
      default:
        // Send data un-altered
        this.createFormSubmission(payload);
    }


  }

  fetchFilesMetadata(data): Observable<ApolloQueryResult<Files_files[]>> {

    const {shot1, shot2, shot3, shot4, shot5} = data.data;
    const imageIds = [shot1, shot2, shot3, shot4, shot5];

    return this.apollo.query<Files_files[]>({
      query: filesForEmailQuery,
      variables: {ids: imageIds}
    });

  }

  createFormSubmission(data) {

    this.apollo.mutate<FormSubmissionView>({
      mutation: createFormSubmissionMutation,
      variables: {formSubmission: data}
    })
      .subscribe(
        () => {
          this.submitted = true;
        },
        (err) => {
          this.toastr.error(err.message, 'Error');
        },
      )

  }

  validateForm() {
    return !this.form.invalid;
  }


}
