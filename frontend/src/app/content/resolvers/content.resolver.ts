import {Observable} from 'rxjs/Observable';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {contentByIdQuery} from '@app/app.graphql';
import {map} from 'rxjs/operators';
import {ContentByIdView} from "@app/apollo/types/ContentByIdView";

@Injectable()
export class ContentResolver implements Resolve<ContentByIdView> {

  constructor(private readonly apollo: Apollo){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ContentByIdView> {
    // TODO error handling

    const {contentId} = route.data;

    return this.apollo.query<ContentByIdView>({
      query: contentByIdQuery,
      variables: { id: contentId },
    }).pipe(map(({data}) => data));

  }



}
