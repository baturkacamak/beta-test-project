import {Observable} from 'rxjs/Observable';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Apollo} from 'apollo-angular';
import {modelsInTownQuery} from '@app/app.graphql';
import {map} from 'rxjs/operators';
import {ModelsInTownQuery} from '@app/apollo/types/ModelsInTownQuery';

@Injectable()
export class ModelsInTownResolver implements Resolve<ModelsInTownQuery> {

  constructor(private readonly apollo: Apollo){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ModelsInTownQuery> {
    // TODO error handling

    const {contentId} = route.data;

    return this.apollo.query<ModelsInTownQuery>({
      query: modelsInTownQuery,
      variables: { id: contentId },
    }).pipe(map(({data}) => data));

  }



}
