import { Component, Input } from '@angular/core';

@Component({
  selector: 'menu-mobile',
  template: `
    <ng-content></ng-content>
    <app-footer></app-footer>
  `,
  styleUrls: ['./menu-mobile.component.sass'],
})
export class MenuMobileComponent {
  @Input()
  isCollapsed: boolean;
  constructor() {}
}
