import {Component, ViewChild} from '@angular/core';
import {RouterOutlet} from '@angular/router';

@Component({
  selector: 'app-layout',
  template: `
    <app-navbar></app-navbar>
    <router-outlet #o="outlet"></router-outlet>
  `
})
export class LayoutComponent {

  @ViewChild('o')
  private outlet: RouterOutlet;

}
