import { Component, HostListener, Inject, Input, OnInit } from '@angular/core';
import { WINDOW } from '@app/window.service';
import { settingListQuery } from '@app/app.graphql';
import { SettingList } from '@app/apollo/types/SettingList';
import { Apollo } from 'apollo-angular';
import { ToastrService } from 'ngx-toastr';
import { animate, style, transition, trigger } from "@angular/animations";

interface SettingsInterface {
  phone: string;
  contactEmail: string;
  address: string;
  about: string;
  facebook: string;
  instagram: string;
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('.2s', style({ opacity: 1 })),
      ]),
      transition(':leave', [animate('.2s', style({ opacity: 0 }))]),
    ]),
  ],
})
export class FooterComponent implements OnInit {

  @Input() noStickyFooter: boolean;

  collapsed: boolean; // = true;
  sticky: boolean;
  private scroll: any;
  private screenWidth: number;
  legalModal: boolean = false;
  legalText: string;

  settings: SettingsInterface;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = window.innerWidth;
    this.onScroll();
  }

  constructor(
    @Inject(WINDOW) private readonly window: Window,
    private readonly apollo: Apollo,
    private readonly toastr: ToastrService
  ) {
    this.scroll = (): void => {
      this.onScroll();
    };

  }

  ngOnInit(): void {

    window.addEventListener('scroll', this.scroll, true);
    // Get window size
    this.screenWidth = window.innerWidth;
    this.onScroll();

    this.apollo.query<SettingList>({
      query: settingListQuery,
    }).subscribe(data => {

      const settings = data.data.settings;

      this.settings = (settings.reduce((memo, next) => {
        memo[next.id] = next.value;
        return memo
      }, {}) as SettingsInterface);

    }, err => this.toastr.error('Failed to load settings.', 'Error'));

  }

  onScroll() {

    if (this.noStickyFooter || this.screenWidth < 993) {
      this.sticky = false;
      return this.collapsed = false;
    }

    this.sticky = true;
    const placeholderHeight = 300; // this.collapsed ? 300 : 1;
    this.collapsed = (window.innerHeight + window.scrollY + placeholderHeight) < document.body.offsetHeight;
  }

  toggleLegalModal = text => {
    this.legalModal = !this.legalModal;
    if (text) this.legalText = text;
  }
}
