import {NgModule} from '@angular/core';
import {APOLLO_OPTIONS, ApolloModule} from 'apollo-angular';
import {HttpLink, HttpLinkModule} from 'apollo-angular-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {ApolloLink} from 'apollo-link';
import {environment} from '@src/environments/environment';

export function createApollo(httpLink: HttpLink) {

  const cache = new InMemoryCache();

  let uri = environment.apiUrl + '/graphql';

  return {
    cache,
    link: ApolloLink.from([httpLink.create({uri})])
  };

}

@NgModule({
  imports: [
    ApolloModule,
    HttpLinkModule
  ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink]
    }
  ]
})
export class TwpApolloModule {
}
