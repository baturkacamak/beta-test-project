/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FormConfigView
// ====================================================

export interface FormConfigView {
  __typename: "FormConfig";
  id: string | null;
  name: string | null;
  config: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}
