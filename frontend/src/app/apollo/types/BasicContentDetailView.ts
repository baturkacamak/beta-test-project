/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: BasicContentDetailView
// ====================================================

export interface BasicContentDetailView_contentType {
  __typename: "ContentType";
  name: string | null;
}

export interface BasicContentDetailView_basicContent {
  __typename: "BasicContent";
  heading: string | null;
  body: string | null;
}

export interface BasicContentDetailView {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  contentType: BasicContentDetailView_contentType | null;
  basicContent: BasicContentDetailView_basicContent | null;
  createdAt: any | null;
  updatedAt: any | null;
}
