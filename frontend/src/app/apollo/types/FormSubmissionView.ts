/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FormSubmissionView
// ====================================================

export interface FormSubmissionView {
  __typename: "FormSubmission";
  id: string | null;
  data: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}
