import {Injectable} from '@angular/core';
import {Routes, RunGuardsAndResolvers} from '@angular/router';
import {tap} from 'rxjs/operators';
import {Apollo} from 'apollo-angular';
import {initialConfigQuery} from '@app/app.graphql';
import {LayoutComponent} from "@app/layout/layout.component";
import {InitialConfig} from '@app/apollo/types/InitialConfig';
import { ModelContentComponent } from '@app/content/model-content/model-content.component';
import { ModelContentResolver } from '@app/content/resolvers/model-content.resolver';
import { ModelsInTownComponent } from '@app/content/models-in-town/models-in-town.component';
import { ModelsInTownResolver } from '@app/content/resolvers/models-in-town.resolver';
import {ModelsByGenderComponent} from "@app/content/models-by-gender/models-by-gender.component";
import {ModelsByGenderResolver} from "@app/content/resolvers/models-by-gender.resolver";
import {FormComponent} from "@app/content/form/form.component";
import {FormResolver} from "@app/content/resolvers/form.resolver";
import {MySelectionComponent} from "@app/content/my-selection/my-selection.component";
import {ContentResolver} from "@app/content/resolvers/content.resolver";

@Injectable()
export class AppConfigService {

  public routes: Routes;
  public settings: { [id: string]: string };

  constructor(private readonly apollo: Apollo) {
  }

  load(): Promise<any> {

    //

    this.routes = [];

    const {apollo} = this;

    return apollo.query<InitialConfig>({
      query: initialConfigQuery,
      fetchPolicy: 'network-only'
    }).pipe(

      tap(({data}) => {

        const { settings } = data;
        this.settings = settings.reduce((memo, next) => {
          memo[next.id] = next.value;
          return memo;
        }, {});

      }),

      tap(({data}) => {

        const { contents } = data;

        const children: Routes = contents
          .map(content => {

            const {contentType, slug, id} = content;

            switch (content.contentType.name) {

              case 'models-in-town':

                return {
                  component: ModelsInTownComponent,
                  path: slug,
                  data: {
                    contentId: id,
                    slug,
                    contentType: contentType.name
                  },
                  resolve: {
                    query: ModelsInTownResolver
                  }
                };


              case 'model':

                return {
                  component: ModelContentComponent,
                  path: slug,
                  data: {
                    contentId: id,
                    slug,
                    contentType: contentType.name
                  },
                  resolve: {
                    content: ModelContentResolver
                  }
                };

              case 'models-by-gender':

                return {
                  component: ModelsByGenderComponent,
                  path: slug,
                  runGuardsAndResolvers: 'paramsOrQueryParamsChange' as RunGuardsAndResolvers,
                  data: {
                    contentId: id,
                    slug,
                    contentType: contentType.name
                  },
                  resolve: {
                    query: ModelsByGenderResolver
                  }
                };

              case 'my-selection':

                return {
                  component: MySelectionComponent,
                  path: slug,
                  data: {
                    contentId: id,
                    slug,
                    contentType: contentType.name
                  },
                  resolve: {
                    content: ContentResolver,
                    form: FormResolver
                  }
                };

              case 'form':

                return {
                  component: FormComponent,
                  path: slug,
                  data: {
                    contentId: id,
                    slug,
                    contentType: contentType.name
                  },
                  resolve: {
                    query: FormResolver
                  }
                };

              default:

                throw new Error('Unexpected content type');

            }

          });

        this.routes.push({
          path: '',
          component: LayoutComponent,
          children
        });

      })
    ).toPromise();
  }

}
