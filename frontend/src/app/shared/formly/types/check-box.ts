import {Component} from '@angular/core';
import {FieldType} from '@ngx-formly/core';


@Component({
  selector: 'formly-check-box',
  template: `
    <div class="form-check">
      <input class="form-check-input" 
             [formControl]="formControl" 
             [formlyAttributes]="field" 
             type="checkbox"
             [id]="field.id">
      <label class="form-check-label" for="{{field.id}}">
        {{ to.checkboxLabel }}
      </label>
    </div>
  `
})
export class FormlyCheckBox extends FieldType {}

