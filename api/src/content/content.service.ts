import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository, IsNull } from 'typeorm';
import { QueryFilter } from '@app/shared/query-filter';
import { ContentEntity } from '@app/orm/entities/content.entity';
import { FileEntity } from '@app/orm/entities/file.entity';
import { ModelContent, PublishState, OldContent } from '@app/graphql/schema';
import { ConfigService } from '@app/shared/config.service';
import { OldContentEntity } from '@app/orm/entities/old-content.entity';



@Injectable()
export class ContentService {

    constructor(
        @InjectRepository(ContentEntity)
        private readonly contentRepository: Repository<ContentEntity>,
        @InjectRepository(FileEntity)
        private readonly fileRepository: Repository<FileEntity>,
        private readonly configService: ConfigService,
        @InjectRepository(OldContentEntity)
        private readonly oldContentRepository: Repository<OldContentEntity>,
    ) {
    }

    async find(filter: QueryFilter): Promise<ContentEntity[]> {

        const { offset, limit, q, contentType } = filter;
        const state = filter.state || PublishState.PUBLISHED;

        const builder = this.contentRepository
            .createQueryBuilder('content')
            .leftJoinAndSelect('content.contentType', 'contentType');
        // TODO subselect only name and id fields

        if (q) {
            builder
                .where('content.name ~* :query')
                .andWhere('contentType.name ~* :query')
                .setParameter('query', `.*${q}.*`);
        }

        if (contentType) {
            builder
                .where('contentType.name = :contentType')
                .setParameter('contentType', contentType);
        }

        switch (state) {
            case PublishState.ALL:
                // Do not add to Query builder, return all content
                break;
            case PublishState.DRAFT:
                builder.andWhere('content.publishedAt is NULL');
                break;
            case PublishState.PUBLISHED:
                builder.andWhere('content.publishedAt is NOT NULL');
                break;
            default:
                throw new Error(`Unhandled publish state: ${state}`);
        }

        return await builder
            .offset(offset)
            .limit(limit)
            .orderBy('content.id', 'ASC')
            .getMany();
    }

    async count(filter: QueryFilter): Promise<number> {

        const { q, contentType } = filter;
        const state = filter.state || PublishState.PUBLISHED;

        const builder = this.contentRepository
            .createQueryBuilder('content')
            .leftJoinAndSelect('content.contentType', 'contentType');
        // TODO subselect only name and id fields

        if (q) {
            builder
                .where('content.name ~* :query')
                .andWhere('contentType.name ~* :query')
                .setParameter('query', `.*${q}.*`);
        }

        if (contentType) {
            builder
                .where('contentType.name = :contentType')
                .setParameter('contentType', contentType);
        }

        switch (state) {
            case PublishState.ALL:
                // Do not add to Query builder, return all content
                break;
            case PublishState.DRAFT:
                builder.andWhere('content.publishedAt is NULL');
                break;
            case PublishState.PUBLISHED:
                builder.andWhere('content.publishedAt is NOT NULL');
                break;
            default:
                throw new Error(`Unhandled publish state: ${state}`);
        }

        return await builder.getCount();
    }

    async create(content: ContentEntity): Promise<ContentEntity> {
        return this.contentRepository.save(content);
    }

    async findById(id: number, state?: PublishState): Promise<ContentEntity | undefined> {

        const query: any = { id };

        if (state) {
            switch (state) {
                case PublishState.ALL:
                    // do nothing
                    break;
                case PublishState.PUBLISHED:
                    query.publishedAt = Not(IsNull());
                    break;
                case PublishState.DRAFT:
                    query.publishedAt = IsNull();
                    break;

            }
        }

        return this.contentRepository.findOne(query);
    }

    async findBySlug(slug: string, state?: PublishState): Promise<ContentEntity | undefined> {

        const query: any = { slug };

        if (state) {
            switch (state) {
                case PublishState.ALL:
                    // do nothing
                    break;
                case PublishState.PUBLISHED:
                    query.publishedAt = Not(IsNull());
                    break;
                case PublishState.DRAFT:
                    query.publishedAt = IsNull();
                    break;

            }
        }

        return this.contentRepository.findOne(query);
    }

    async modelsInTown(): Promise<ContentEntity[]> {
        return this.contentRepository.createQueryBuilder('c')
            .where(`c.data ::jsonb @> '{"inTown": true}'`)
            .andWhere('c.publishedAt IS NOT NULL')
            .getMany();
    }

    async modelsByGender(gender: string, firstLetter?: string): Promise<ContentEntity[]> {

        const builder = this.contentRepository.createQueryBuilder('c')
            .where('c.content_type_id = 1')
            .andWhere(`c.data ::jsonb @> '{"gender": "${gender}"}'`)
            .andWhere('c.publishedAt IS NOT NULL');

        if (firstLetter) {
            builder.andWhere(`LOWER(c.data->>'name') LIKE LOWER(:firstLetter)`)
                .setParameter('firstLetter', `${firstLetter}%`);
        }

        builder.orderBy('c.data->>\'name\'', 'ASC');

        return builder.getMany();
    }

    async selectedModels(ids: string[]): Promise<ContentEntity[]> {

        return this.contentRepository.createQueryBuilder('c')
            .where('c.id IN (:...ids)')
            .andWhere('c.publishedAt IS NOT NULL')
            .setParameter('ids', ids)
            .getMany();

    }

    async updateById(id: number, content: ContentEntity): Promise<ContentEntity | undefined> {

        const count = await this.contentRepository.count({ id });
        if (count === 0) return undefined;

        await this.contentRepository.update(id, content);

        return await this.findById(id);
    }

    async deleteById(id: number): Promise<boolean> {

        const count = await this.contentRepository.count({ id });

        if (count === 0) return false;

        await this.contentRepository.delete(id, {});

        return true;
    }

    async loadOldData(): Promise<OldContentEntity[]> {
        const hair_colors = {
            1: "Blonde",
            2: "Brown",
            3: "Red",
            4: "Black",
            5: "Dark Blonde",
            6: "Dark Brown",
            7: "Light Blonde",
            8: "Light Brown",
            9: "Salt and Pepper",
            10: "Other"
        }

        const eye_color = {
            1: 'Blue',
            2: 'Green',
            3: 'Brown',
            4: 'Honey',
            5: 'Black',
            6: 'Grey',
            7: 'Turquoise',
            8: 'Hazel',
            9: 'Other'
        }

        const categories = {
            "1": "Women",
            "2": "Men",
            "3": "Senior"
        }

        const catAssign = {
            1: "female",
            2: "female",
            3: "female",
            4: "female",
            5: "female",
            6: "female",
            7: "female",
            10: "female",
            12: "female",
            16: "female",
            17: "female",
            18: "female",
            19: "female",
            20: "female",
            22: "female",
            23: "female",
            24: "female",
            25: "female",
            26: "female",
            28: "female",
            29: "female",
            30: "female",
            31: "female",
            32: "female",
            33: "female",
            36: "female",
            37: "female",
            38: "female",
            39: "female",
            40: "female",
            41: "female",
            42: "female",
            43: "female",
            45: "female",
            46: "female",
            47: "female",
            48: "male",
            49: "male",
            50: "male",
            52: "male",
            53: "male",
            54: "male",
            55: "male",
            57: "male",
            58: "male",
            61: "male",
            62: "male",
            63: "male",
            65: "male",
            67: "male",
            68: "male",
            69: "male",
            70: "male",
            72: "male",
            73: "male",
            74: "male",
            75: "male",
            76: "male",
            77: "male",
            78: "male",
            79: "male",
            80: "male",
            81: "male",
            82: "male",
            84: "male",
            85: "male",
            88: "male",
            89: "male",
            90: "male",
            91: "male",
            92: "male",
            93: "male",
            94: "male",
            200: "male",
            201: "male",
            202: "male",
            204: "male",
            205: "female",
            206: "female",
            207: "female",
            208: "female",
            209: "male",
            210: "male",
            214: "female",
            215: "male",
            216: "male",
            217: "male",
            218: "female",
            220: "male",
            221: "male",
            222: "male",
            223: "female",
            224: "female",
            225: "male",
            226: "male",
            227: "female",
            228: "female",
            230: "male",
            231: "male",
            235: "male",
            236: "male",
            237: "male",
            239: "male",
            240: "male",
            242: "male",
            244: "female",
            246: "male",
            247: "female",
            250: "male",
            251: "female",
            252: "female",
            253: "male",
            254: "female",
            255: "female",
            259: "female",
            262: "male",
            263: "female",
            264: "male",
            265: "male",
            270: "female",
            271: "female",
            272: "male",
            273: "male",
            274: "female",
            275: "male",
            278: "male",
            279: "male",
            281: "female",
            282: "male",
            283: "female",
            284: "female",
            285: "female",
            286: "male",
            288: "female",
            289: "male",
            292: "female",
            293: "female",
            294: "male",
            295: "male",
            296: "female",
            298: "female",
            300: "male",
            301: "male",
            302: "male",
            303: "female",
            304: "male",
            305: "female",
            306: "male",
            307: "male",
            308: "female",
            309: "female",
            312: "male",
            314: "male",
            315: "male",
            316: "male",
            320: "female",
            323: "female",
            324: "male",
            325: "female",
            327: "female",
            328: "female",
            330: "male",
            331: "male",
            333: "male",
            334: "female",
            335: "male",
            337: "female",
            338: "male",
            339: "male",
            340: "male",
            341: "male",
            342: "male",
            343: "male",
            344: "male",
            345: "female",
            346: "female",
            347: "female",
            348: "female",
            349: "female",
            351: "female",
            352: "female",
            353: "male",
            354: "male",
            355: "male",
            356: "male",
            357: "male",
            358: "male",
            359: "female",
            360: "male",
            361: "male",
            362: "female",
            364: "male",
            365: "female",
            366: "female",
            367: "female",
            369: "male",
            370: "female",
            371: "male",
            372: "male",
            374: "male",
            375: "male",
            376: "female",
            377: "female",
            378: "male",
            379: "male",
            380: "female",
            381: "male",
            383: "male",
            384: "male",
            385: "male",
            386: "male",
            387: "male",
            388: "male",
            390: "male",
            391: "female",
            393: "female",
            394: "male",
            395: "male",
            396: "female",
            397: "male",
            398: "male",
            402: "female",
            403: "male",
            404: "male",
            406: "male",
            407: "female",
            409: "female",
            410: "male",
            411: "female",
            412: "male",
            413: "female",
            414: "female",
            415: "female",
            418: "male",
            419: "female",
            421: "female",
            423: "female",
            424: "female",
            425: "male",
            426: "male",
            427: "male",
            430: "female",
            431: "male",
            433: "female",
            434: "male",
            435: "male",
            437: "female",
            438: "female",
            440: "female",
            441: "female",
            442: "male",
            443: "male",
            444: "female",
            445: "female",
            446: "female",
            447: "male",
            448: "male",
            449: "male",
            451: "female",
            452: "female",
            453: "female",
            454: "male",
            456: "female",
            458: "male",
            461: "female",
            463: "male",
            464: "male",
            465: "female",
            467: "female",
            468: "male",
            469: "female",
            471: "male",
            472: "female",
            474: "female",
            475: "male",
            476: "male",
            478: "female",
            480: "male",
            482: "female",
            483: "female",
            484: "female",
            485: "male",
            486: "male",
            487: "female",
            488: "female",
            489: "female",
            490: "female",
            491: "male",
            492: "female",
            493: "male",
            494: "female",
            495: "female",
            496: "female",
            497: "male",
            498: "male",
            499: "male",
            500: "male",
            501: "female",
            503: "female",
            505: "female",
            506: "female",
            507: "female",
            508: "female",
            509: "male",
            510: "male",
            511: "female",
            513: "female",
            514: "female",
            515: "male",
            516: "male",
            518: "female",
            519: "male",
            520: "female",
            521: "female",
            522: "male",
            523: "male",
            524: "male",
            525: "female",
            526: "female",
            527: "female",
            529: "female",
            530: "male",
            532: "female",
            533: "female",
            534: "female",
            536: "female",
            537: "female",
            538: "male",
            539: "female",
            540: "male",
            541: "female",
            543: "female",
            545: "female",
            547: "male",
            549: "female",
            550: "female",
            551: "male",
            552: "female",
            553: "female",
            554: "male",
            555: "female",
            558: "female",
            559: "male",
            560: "male",
            561: "female",
            563: "male",
            564: "female",
            565: "male",
            566: "male",
            567: "female",
            568: "male",
            569: "male",
            571: "female",
            572: "male",
            573: "male",
            574: "male",
            575: "female",
            577: "female",
            578: "male",
            580: "male",
            582: "female",
            583: "male",
            584: "female",
            587: "female",
            589: "female",
            590: "male",
            591: "female",
            592: "female",
            593: "female",
            594: "male",
            595: "female",
            599: "male",
            600: "female",
            601: "female",
            602: "female",
            604: "male",
            605: "female",
            606: "male",
            607: "male",
            608: "female",
            609: "female",
            610: "female",
            611: "male",
            613: "female",
            614: "female",
            615: "female",
            616: "female",
            617: "male",
            618: "male",
            619: "male",
            621: "female",
            622: "female",
            623: "female",
            624: "male",
            625: "male",
            627: "female",
            628: "female",
            629: "male",
            630: "female",
            631: "female",
            632: "female",
            633: "male",
            634: "male",
            637: "female",
            639: "male",
            640: "female",
            641: "female",
            642: "female",
            643: "female",
            644: "male",
            645: "female",
            646: "male",
            647: "male",
            648: "female",
            650: "female",
            651: "female",
            654: "male",
            655: "male",
            656: "female",
            657: "female",
            658: "female",
            659: "female",
            661: "female",
            662: "female",
            663: "female",
            664: "female",
            665: "male",
            667: "female",
            668: "female",
            669: "male",
            670: "female",
            671: "female",
            672: "female",
            674: "male",
            675: "female",
            676: "female",
            677: "female",
            678: "female",
            679: "male",
            680: "female",
            681: "male",
            682: "female",
            683: "female",
            684: "female",
            685: "female",
            686: "female",
            687: "female",
            688: "female",
            689: "male",
            691: "female",
            692: "female",
            693: "female",
            694: "female",
            695: "male",
            696: "male",
            697: "male",
            698: "female",
            699: "female",
            701: "female",
            702: "female",
            703: "male",
            704: "female",
            705: "female",
            706: "male",
            707: "female",
            708: "female",
            709: "female",
            710: "male",
            711: "female",
            712: "male",
            713: "female",
            714: "female",
            715: "female",
            716: "female",
            717: "female",
            718: "female",
            719: "male",
            720: "female",
            721: "female",
            722: "female",
            723: "female",
            724: "female",
            725: "female",
            726: "female",
            727: "female",
            728: "female",
            729: "female"
        }


        let content =  await this.oldContentRepository.createQueryBuilder('jos_scatalog_products').getMany();

        for (let c of content) {
            let newContent = new ContentEntity({
                title: c.title,
                meta: [{"key": "description", "value": "Test description"}],
                slug: c.alias,
                contentTypeId: 1,
                publishedAt: new Date(),
                createdAt: new Date(),
                updatedAt: new Date(),
                data: {
                    "bust":parseFloat(c.bust),
                    "hips":parseFloat(c.hips),
                    "name":c.title,
                    "shoes":parseFloat(c.shoe_size),
                    "waist":parseFloat(c.waist),
                    "gender":catAssign[c.id],
                    "height": parseFloat(c.height)/100,
                    "inTown": false,
                    "eyeColor": eye_color[c.eye_colour],
                    "hairColor":hair_colors[c.hair_colour],
                    "instagram":"",
                    "images":[],
                    "country":""
                }
            });

            this.contentRepository.save(newContent);
        };
        
        return content;
    }

    generatePdfHtml(model: ModelContent, images): string {

        const baseUrl = this.configService.baseUrls.pdfAssets;

        return `
            <html>
                <head>                                  
                    <base href="${baseUrl}"/>                  
                    <link rel="stylesheet" href="/assets/model-pdf.css">
                </head>
                <body>
                <div class="page black-bg" style="text-align: center;">
                    <img class="logo" src="/assets/img/logo-pink.svg">
                    <h1>${model.name}</h1>
                    <ul class="inline-list">
                        <li>Height - ${model.height}</li>
                        <li>Bust - ${model.bust}</li>
                        <li>Waist - ${model.waist}</li>
                        <li>Hips - ${model.hips}</li>
                        <li>Shoes - ${model.shoes}</li>
                        <li>Eyes - ${model.eyeColor}</li>
                        <li>Hair - ${model.hairColor}</li>
                    </ul>
                </div>
                
                <div class="page pink-bg">
                    <div class="left-img" style="background-image: url(${images[0].metadata.secure_url});">
                    </div>
                    <div class="right-img" style="background-image: url(${images[1].metadata.secure_url});">
                    </div>
                    <img class="logo" style="padding-top: 40px" src="/assets/img/logo.svg">
                </div>
                
                <div class="page black-bg">
                    <div class="row" style="padding-top: 300px; margin: 40px auto 60px">
                        <div class="left-col">
                            <img src="/assets/img/ta.jpg" height="60" style="padding-left: 143px" />
                        </div>
                        <div class="right-col">
                            <img src="/assets/img/ber.jpg" height="60" style="padding-left: 143px" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="left-col">
                
                        </div>
                        <div class="right-col">
                            <ul>
                                <li>Carrer Monterols 6, sobre-&agrave;tic</li>
                                <li>08034 Barcelona, Spain</li>
                                <li>&nbsp;</li>
                                <li>+(34) 625678168</li>
                                <li>berta@bertamodels.com</li>
                                <li>bertamodels.com</li>
                            </ul>
                        </div>
                    </div>
                </div>
                </body>
            </html>
        `;

    }

}
