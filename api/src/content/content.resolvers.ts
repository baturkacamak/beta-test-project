import { Args, Mutation, Parent, Query, ResolveProperty, Resolver } from '@nestjs/graphql';

import { ContentService } from '@app/content/content.service';
import { UseGuards, ValidationPipe } from '@nestjs/common';
import { UpsertContentDto } from '@app/content/dto/upsert-content.dto';
import { ContentEntity } from '@app/orm/entities/content.entity';
import { ContentDto } from '@app/content/dto/content.dto';
import { JwtAuthGuard } from '@app/auth/guards/jwt-auth.guard';
import { ModelContentDto } from '@app/content/dto/model-content.dto';
import { FormContentDto } from '@app/content/dto/form-content.dto';
import { PublishState } from '@app/graphql/schema';

export type ContentTypeName = 'model' | 'about' | 'form';

@Resolver('Content')
export class ContentResolvers {

  constructor(private readonly contentService: ContentService) {
  }

  @Query()
  @UseGuards(JwtAuthGuard)
  async contentsAdmin(@Args('offset') offset: number,
    @Args('limit') limit: number,
    @Args('q') q?: string,
    @Args('contentType') contentType?: string,
    @Args('state') state?: PublishState) {
    return await this.contents(offset, limit, q, contentType, state);
  }

  @Query()
  @UseGuards(JwtAuthGuard)
  async contentsAdminCount(@Args('offset') offset: number,
    @Args('limit') limit: number,
    @Args('q') q: string,
    @Args('contentType') contentType: string,
    @Args('state') state: PublishState) {
    return await this.contentsCount(offset, limit, q, contentType, state);
  }

  @Query()
  async contents(@Args('offset') offset: number,
    @Args('limit') limit: number,
    @Args('q') q?: string,
    @Args('contentType') contentType?: string,
    state?: PublishState) {
    // TODO set cache control

    const entities = await this.contentService.find({ offset, limit, q, contentType, state });
    return entities.map(e => new ContentDto(e));
  }

  @Query()
  async contentsCount(@Args('offset') offset: number,
    @Args('limit') limit: number,
    @Args('q') q: string,
    @Args('contentType') contentType: string,
    state?: PublishState) {
    // TODO set cache control
    return await this.contentService.count({ offset, limit, q, contentType, state });
  }

  @Query()
  async contentAdmin(@Args('id') id: string) {
    return await this.content(id, PublishState.ALL);
  }

  @Query()
  async contentBySlugAdmin(@Args('slug') slug: string) {
    return await this.contentBySlug(slug, PublishState.ALL);
  }

  @Query()
  async content(@Args('id') id: string, state: PublishState = PublishState.PUBLISHED) {
    if (id === 'new') {
      return { id, data: {} };
    }
    // TODO set cache control
    const entity = await this.contentService.findById(+id, state);
    return entity ? new ContentDto(entity) : null;
  }

  @Query()
  async contentBySlug(@Args('slug') slug: string, state: PublishState = PublishState.PUBLISHED) {
    // TODO set cache control
    const entity = await this.contentService.findBySlug(slug, state);
    return entity ? new ContentDto(entity) : null;
  }

  @Query()
  async modelsInTown() {
    const contents = await this.contentService.modelsInTown();
    return contents.map(c => new ContentDto(c));
  }

  @Query()
  async modelsByGender(@Args('gender') gender: string,
    @Args('firstLetter') firstLetter: string) {
    const contents = await this.contentService.modelsByGender(gender, firstLetter);
    return contents.map(c => new ContentDto(c));
  }

  @Query()
  async selectedModels(@Args('ids') ids: string[]) {
    const contents = await this.contentService.selectedModels(ids);
    return contents.map(c => new ContentDto(c));
  }

  @ResolveProperty()
  modelContent(@Parent() content: ContentDto) {
    return this.resolveContent('model', content);
  }

  @ResolveProperty()
  formContent(@Parent() content: ContentDto) {
    return this.resolveContent('form', content);
  }

  resolveContent(contentTypeName: ContentTypeName, content: ContentDto) {
    const { contentType, data } = content;

    const id = (contentType && contentType.name) ? contentType.name : contentTypeName;

    switch (id) {
      case 'model':
        return new ModelContentDto(data);
      case 'form':
        return new FormContentDto(data);
    }
  }

  @Mutation()
  @UseGuards(JwtAuthGuard)
  async createContent(@Args('content', new ValidationPipe({ groups: ['create'] })) content: UpsertContentDto) {
    const entity = new ContentEntity(content);
    return new ContentDto(await this.contentService.create(entity));
  }

  @Mutation()
  @UseGuards(JwtAuthGuard)
  async updateContentById(@Args('id') id: number,
    @Args('content', new ValidationPipe({ groups: ['create'] })) content: Partial<UpsertContentDto>) {
    const entity = new ContentEntity(content);
    return new ContentDto(await this.contentService.updateById(id, entity));
  }

  @Mutation()
  @UseGuards(JwtAuthGuard)
  async deleteContentById(@Args('id') id: number) {
    return await this.contentService.deleteById(id);
  }

  @Mutation()
  async loadOldData() {
    return await this.contentService.loadOldData();
  }

}
