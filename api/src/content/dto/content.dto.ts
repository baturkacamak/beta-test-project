import {Content} from '@app/graphql/schema';
import {assignClean} from '@app/shared/utils';
import {ContentTypeDto} from '@app/content/dto/content-type.dto';


export class ContentDto extends Content {

    constructor(data: any) {
        super();
        assignClean(this, data);

        if(this.contentType) {
            this.contentType = new ContentTypeDto(this.contentType);
        }
    }

}
