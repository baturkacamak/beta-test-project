import {UpsertContentType} from '@app/graphql/schema';
import {assignClean} from '@app/shared/utils';

export class UpsertContentTypeDto extends UpsertContentType {

    constructor(data: any) {
        super();
        assignClean(this, data);
    }


}
