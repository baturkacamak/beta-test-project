import { assignClean } from '@app/shared/utils';
import { UpsertFormSubmission } from '@app/graphql/schema';
import { FormConfigDto } from '@app/forms/dto/form-config.dto';

export class UpsertFormSubmissionDto extends UpsertFormSubmission {

	constructor(data: any) {
		super();

		if (data && data.formConfig) data.formConfig = new FormConfigDto(data.formConfig);

		assignClean(this, data);
	}

}
