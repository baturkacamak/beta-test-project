/* tslint:disable */
export enum PublishState {
    ALL = "ALL",
    PUBLISHED = "PUBLISHED",
    DRAFT = "DRAFT"
}

export class UpdateSetting {
    id?: string;
    value?: string;
}

export class UpsertContent {
    title?: string;
    meta?: MetaTags;
    slug?: string;
    contentTypeId?: string;
    data?: JSON;
    publishedAt?: Date;
    modelContent?: UpsertModelContent;
}

export class UpsertContentType {
    name?: string;
    slug?: string;
    formConfig?: JSON;
}

export class UpsertFile {
    clTransforms?: string;
}

export class UpsertFormConfig {
    name?: string;
    config?: JSON;
}

export class UpsertFormSubmission {
    data?: JSON;
    formConfigId?: string;
}

export class UpsertModelContent {
    name?: string;
    email?: string;
    gender?: string;
    country?: string;
    inTown?: boolean;
    height?: number;
    bust?: number;
    waist?: number;
    hips?: number;
    shoes?: number;
    eyeColor?: string;
    hair?: string;
    instagram?: string;
}

export class UpsertUser {
    email?: string;
    firstName?: string;
    familyName?: string;
    password?: string;
}

export class AccessToken {
    id?: string;
    expiresIn?: number;
    user?: User;
}

export class Content {
    id?: string;
    title?: string;
    meta?: MetaTags;
    slug?: string;
    contentType?: ContentType;
    data?: JSON;
    modelContent?: ModelContent;
    formContent?: FormContent;
    publishedAt?: Date;
    createdAt?: Date;
    updatedAt?: Date;
}

export class ContentType {
    id?: string;
    name?: string;
    slug?: string;
    formConfig?: JSON;
    createdAt?: Date;
    updatedAt?: Date;
}

export class EmailConfig {
    emailRecipients?: EmailRecipients;
    subject?: string;
}

export class EmailRecipients {
    emails?: string[];
    fields?: string[];
}

export class File {
    id?: string;
    mimetype?: string;
    filename?: string;
    metadata?: JSON;
    createdAt?: Date;
    updatedAt?: Date;
    owner?: User;
    clTransforms?: string;
}

export class FormConfig {
    id?: string;
    name?: string;
    config?: JSON;
    emailConfig?: JSON;
    emailConfigData?: EmailConfig;
    createdAt?: Date;
    updatedAt?: Date;
}

export class FormContent {
    title?: string;
    subtitle?: string;
    buttonText?: string;
    submitted?: string;
    formConfigId?: number;
}

export class FormSubmission {
    id?: string;
    data?: JSON;
    formConfig?: FormConfig;
    createdAt?: Date;
    updatedAt?: Date;
}

export class ModelContent {
    name?: string;
    email?: string;
    gender?: string;
    country?: string;
    inTown?: boolean;
    height?: number;
    bust?: number;
    waist?: number;
    hips?: number;
    shoes?: number;
    eyeColor?: string;
    hairColor?: string;
    instagram?: string;
    images?: File[];
}

export abstract class IMutation {
    abstract authenticateWithEmail(email: string, password: string): AccessToken | Promise<AccessToken>;

    abstract requestPasswordReset(email: string): boolean | Promise<boolean>;

    abstract resetPassword(password: string): boolean | Promise<boolean>;

    abstract createContentType(contentType?: UpsertContentType): ContentType | Promise<ContentType>;

    abstract updateContentTypeById(id: string, content?: UpsertContentType): ContentType | Promise<ContentType>;

    abstract deleteContentTypeById(id: string): boolean | Promise<boolean>;

    abstract createContent(content?: UpsertContent): Content | Promise<Content>;

    abstract updateContentById(id: string, content?: UpsertContent): Content | Promise<Content>;

    abstract deleteContentById(id: string): boolean | Promise<boolean>;

    abstract loadOldData(input?: string): OldContent[] | Promise<OldContent[]>;

    abstract updateFileById(id: string, file?: UpsertFile): File | Promise<File>;

    abstract createFormConfig(formConfig?: UpsertFormConfig): FormConfig | Promise<FormConfig>;

    abstract updateFormConfigById(id: string, formConfig?: UpsertFormConfig): FormConfig | Promise<FormConfig>;

    abstract deleteFormConfigById(id: string): boolean | Promise<boolean>;

    abstract createFormSubmission(formSubmission?: UpsertFormSubmission): FormSubmission | Promise<FormSubmission>;

    abstract updateFormSubmissionById(id: string, formSubmission?: UpsertFormSubmission): FormSubmission | Promise<FormSubmission>;

    abstract deleteFormSubmissionById(id: string): boolean | Promise<boolean>;

    abstract updateSettings(settings?: UpdateSetting[]): Setting[] | Promise<Setting[]>;

    abstract createUser(user?: UpsertUser): User | Promise<User>;

    abstract updateUserById(id: string, user?: UpsertUser): User | Promise<User>;

    abstract deleteUserById(id: string): boolean | Promise<boolean>;
}

export class OldContent {
    title?: string;
    alias?: string;
    short_desc?: string;
    descr?: string;
    image?: string;
    gallery_id?: number;
    manufacturer_id?: number;
    metakey?: string;
    metadesc?: string;
    ordering?: number;
    created?: Date;
    published?: boolean;
    checked_out?: number;
    checked_out_time?: Date;
    recommended?: boolean;
    hits?: number;
    default_image?: string;
    in_town?: boolean;
    new_face?: boolean;
    height?: string;
    bust?: string;
    waist?: string;
    hips?: string;
    shoe_size?: string;
    hair_colour?: number;
    eye_colour?: number;
    race?: number;
    pdf?: string;
}

export abstract class IQuery {
    abstract contentTypes(offset?: number, limit?: number, q?: string): ContentType[] | Promise<ContentType[]>;

    abstract contentTypesCount(offset?: number, limit?: number, q?: string): number | Promise<number>;

    abstract contentType(id: string): ContentType | Promise<ContentType>;

    abstract contentsAdmin(offset?: number, limit?: number, q?: string, contentType?: string, state?: PublishState): Content[] | Promise<Content[]>;

    abstract contentsAdminCount(offset?: number, limit?: number, q?: string, contentType?: string, state?: PublishState): number | Promise<number>;

    abstract contentAdmin(id: string): Content | Promise<Content>;

    abstract contentBySlugAdmin(slug: string): Content | Promise<Content>;

    abstract contents(offset?: number, limit?: number, q?: string, contentType?: string): Content[] | Promise<Content[]>;

    abstract contentsCount(offset?: number, limit?: number, q?: string, contentType?: string): number | Promise<number>;

    abstract content(id: string): Content | Promise<Content>;

    abstract contentBySlug(slug: string): Content | Promise<Content>;

    abstract modelsInTown(): Content[] | Promise<Content[]>;

    abstract modelsByGender(gender: string, firstLetter?: string): Content[] | Promise<Content[]>;

    abstract selectedModels(ids?: string[]): Content[] | Promise<Content[]>;

    abstract file(id: string): File | Promise<File>;

    abstract files(ids?: string[]): File[] | Promise<File[]>;

    abstract formConfigs(offset?: number, limit?: number, q?: string): FormConfig[] | Promise<FormConfig[]>;

    abstract formConfigsCount(offset?: number, limit?: number, q?: string): number | Promise<number>;

    abstract formConfig(id: string): FormConfig | Promise<FormConfig>;

    abstract formSubmissions(offset?: number, limit?: number, q?: string): FormSubmission[] | Promise<FormSubmission[]>;

    abstract formSubmissionsCount(offset?: number, limit?: number, q?: string): number | Promise<number>;

    abstract formSubmission(id: string): FormSubmission | Promise<FormSubmission>;

    abstract settings(offset?: number, limit?: number, q?: string): Setting[] | Promise<Setting[]>;

    abstract settingsCount(offset?: number, limit?: number, q?: string): number | Promise<number>;

    abstract setting(id: string): Setting | Promise<Setting>;

    abstract users(offset?: number, limit?: number, q?: string): User[] | Promise<User[]>;

    abstract usersCount(offset?: number, limit?: number, q?: string): number | Promise<number>;

    abstract user(id: string): User | Promise<User>;

    abstract temp__(): boolean | Promise<boolean>;
}

export class Setting {
    id?: string;
    value?: string;
    createdAt?: Date;
    updatedAt?: Date;
}

export class User {
    id?: string;
    email?: string;
    firstName?: string;
    familyName?: string;
    createdAt?: Date;
    updatedAt?: Date;
}

export type ContentData = any;
export type Date = any;
export type JSON = any;
export type MetaTags = any;
