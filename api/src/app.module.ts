import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {SharedModule} from '@app/shared/shared.module';
import {GraphQLModule} from '@app/graphql/graphql.module';
import {OrmModule} from '@app/orm/orm.module';
import {AuthModule} from '@app/auth/auth.module';
import {UserModule} from '@app/user/user.module';
import {ContentModule} from '@app/content/content.module';
import {FileModule} from '@app/file/file.module';
import {SettingModule} from '@app/setting/setting.module';

import Cors from 'cors';
import helmet from 'helmet';
import csurf from 'csurf';
import rateLimit from 'express-rate-limit';
import {ConfigService} from "@app/shared/config.service";
import {FormsModule} from "@app/forms/forms.module";

const cloudinary = require('cloudinary');

@Module({
    imports: [
        SharedModule,
        GraphQLModule,
        OrmModule,
        AuthModule,
        UserModule,
        ContentModule,
        FileModule,
        SettingModule,
        FormsModule
    ]
})
export class AppModule implements NestModule {

    constructor(private readonly configService: ConfigService){}

    configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
        this.configureCors(consumer);
        this.configureHelmet(consumer);
        this.configureCsurf(consumer);
        this.configureRateLimiter(consumer);
        this.configureCloudinary();
    }

    private configureCors(consumer: MiddlewareConsumer) {
        // set cors headers
        consumer
            .apply(Cors())
            .forRoutes('*');
    }

    private configureHelmet(consumer: MiddlewareConsumer) {
        consumer.apply(helmet());
    }

    private configureCsurf(consumer: MiddlewareConsumer) {
        consumer.apply(csurf());
    }

    private configureRateLimiter(consumer: MiddlewareConsumer) {
        consumer.apply(rateLimit({
            windowMs: 60 * 1000, // 1 minute
            max: 200 // limit each IP to 200 requests per windowMs
        }))
    }

    configureCloudinary() {

        const config = this.configService.config;

        cloudinary.config({
            cloud_name: config.get('cloudinary.cloud'),
            api_key: config.get('cloudinary.key'),
            api_secret: config.get('cloudinary.secret'),
        });
    }
}
