import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ParseIntPipe, ValidationPipe } from '@nestjs/common';
import { FileService } from './file.service';
import { FileDto } from '@app/file/dto/file.dto';
import { FileEntity } from '@app/orm/entities/file.entity';
import { UpsertFileDto } from './dto/upsert-file.dto';

@Resolver('File')
export class FileResolvers {

    constructor(private readonly fileService: FileService) { }

    @Query()
    async file(@Args('id', ParseIntPipe) id: number) {
        const entity = await this.fileService.findOne(id);
        return entity ? new FileDto(entity) : null;
    }

    @Query()
    async files(@Args('ids') ids: number[]) {
        const entities = await this.fileService.findByIds(ids);
        return entities.map(entity => new FileDto(entity));
    }

    @Mutation()
    async updateFileById(@Args('id') id: number,
        @Args('file', new ValidationPipe({ groups: ['update'] })) dto: UpsertFileDto) {
        const entity = new FileEntity(dto);
        return new FileDto(await this.fileService.updateById(id, entity));
    }

}
