import {File} from '@app/graphql/schema';
import {assignClean} from '@app/shared/utils';

export class FileDto extends File {

    constructor(data: any) {
        super();
        assignClean(this, data);
    }

}
