import { assignClean } from '@app/shared/utils';
import { UpsertFile } from '@app/graphql/schema';

export class UpsertFileDto extends UpsertFile {

  constructor(data: any) {
    super();
    assignClean(this, data);
  }

}
