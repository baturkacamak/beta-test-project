import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { assignClean } from '../../shared/utils';
import { FormSubmissionEntity } from '@app/orm/entities/form-submission.entity';

@Entity('form_config')
export class FormConfigEntity {
	constructor(data: any) {
		assignClean(this, data);
	}

	@PrimaryGeneratedColumn('increment')
	id: number;

	@Column({ length: 64 })
	name: string;

	@Column({ type: 'jsonb' })
	config: any;

	@Column({ type: 'jsonb' })
	emailConfig: any;

	@Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
	createdAt: Date;

	@Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
	updatedAt: Date;

	@OneToMany(type => FormSubmissionEntity, formSubmission => formSubmission.formConfig)
	formSubmissions: FormSubmissionEntity[];
}
