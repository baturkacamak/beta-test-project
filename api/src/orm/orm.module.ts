import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SnakeCaseNamingStrategy } from '@app/orm/config';
import { ConfigService } from '@app/shared/config.service';
import { ConnectionOptions } from 'typeorm';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            useFactory: async (configService: ConfigService): Promise<ConnectionOptions> => {

                const { db } = configService;
                const { url, ssl } = db;

                const ormConfig: any = {
                    type: 'postgres',
                    url: url,
                    synchronize: false,
                    entities: ['src/**/**.entity{.ts,.js}'],
                    namingStrategy: new SnakeCaseNamingStrategy(),
                    logging: ['error']
                };

                if (ssl.enabled) {
                    ormConfig.ssl = {
                        ca: ssl.ca
                    }
                }

                return ormConfig as ConnectionOptions
            },
            inject: [ConfigService]
        }),
    ],
})
export class OrmModule {
}
